import { RegController } from "../lib/controllers/register.controller";
import { checkResponseTime, checkStatusCode, checkObjectIsExiting } from "../../helpers/functionsForChecking.helper";

const newUser = new RegController();

it(`register new user`, async () => {

    let userData: object = {
        id: 0,
        avatar: "string",
        email: "email.qa.newtest@gmail.com",
        userName: "new_user",
        password: "password123",
    };

    let response = await newUser.registry(userData);

    checkStatusCode(response, 200);
    checkResponseTime(response, 3000);
    checkObjectIsExiting(response, 1);
    
});

