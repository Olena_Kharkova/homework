import { PostsController } from "../lib/controllers/posts.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkResponseTime, checkStatusCode, checkObjectIsExiting } from "../../helpers/functionsForChecking.helper";


const users = new PostsController();
const auth = new AuthController();
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe(`Posts controller`, () => {

    it(`should return 200 status code and all posts when getting the posts collection`, async () => {
        let response = await users.getAllPosts();


        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkObjectIsExiting(response, 1);
    });

   
});

