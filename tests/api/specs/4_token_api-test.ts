import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkResponseTime, checkStatusCode, checkObjectIsExiting } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const auth = new AuthController();

xdescribe(`Users controller with token`, () => {
    let accessToken: string;

    before(`Get the token`, async () => {
        let response = await auth.login("email.qa.test@gmail.com", "password");

        accessToken = response.body.token.accessToken.token;

    });

    it(`should return user details when getting user details with valid token`, async () => {

        let response = await users.getUserByToken(accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);

    });

    it(`should return 401 error when getting user details with invalid token`, async () => {
        let invalidToken = '12345'

        let response = await users.getUserByToken(invalidToken);

        checkStatusCode(response, 401);
        checkResponseTime(response, 1000);
    });

    it(`should return 200 status code and user data will be updated`, async () => {
        let newUserData = {
            userName: "New_test_name"
        };
        let response = await users.updateUser(newUserData, accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
    });

    afterEach(function(){
        console.log('It was a test')
    });
});
