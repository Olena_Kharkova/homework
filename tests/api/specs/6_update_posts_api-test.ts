import { AuthController } from "../lib/controllers/auth.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { PostsController } from "../lib/controllers/posts.controller";
import { checkResponseTime, checkStatusCode, checkObjectIsExiting } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const posts = new PostsController();
const auth = new AuthController();

xdescribe(`Posts controller with token`, () => {

    let accessToken = "string";
    let userId = 0;

    it(`should return 200 status code when post is created`, async () => {

        let postData: object = {
            authorId: userId,
            previewImage: "string",
            body: "string"
        };

        let response = await posts.createPost(postData, accessToken);


        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
    });

    it(`should return 200 status code and all posts when getting the posts collection for cheking of creation`, async () => {
        let response = await posts.getAllPosts();


        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkObjectIsExiting(response, 1);
    });

    it(`should return 200 status code when post is liked`, async () => {
        let postData: object = {
            entityId: 0,
            isLike: true,
            userId: 0
        }

        let response = await posts.likePost(postData, accessToken);


        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkObjectIsExiting(response, 1);
    });

    it(`should return 200 status code and all posts when getting the posts collection for cheking that post is liked`, async () => {
        let response = await posts.getAllPosts();


        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkObjectIsExiting(response, 1);
    });

    it(`should return 200 status code when post is commented`, async () => {
        let postData: object = {
            authorId: 0,
            postId: 0,
            body: "string"
        }

        let response = await posts.commentPost(postData, accessToken);


        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);

    });

    it(`should return 200 status code and all posts when getting the posts collection for cheking that post is commented`, async () => {
        let response = await posts.getAllPosts();


        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkObjectIsExiting(response, 1);
    });

    afterEach(function(){
        console.log('It was a test')
    });
});