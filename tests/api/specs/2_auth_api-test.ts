import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkResponseTime, checkStatusCode, checkObjectIsExiting } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const auth = new AuthController();

xdescribe("Token usage", () => {
    let accessToken: string;

    before(`Login and get the token`, async () => {
        let response = await auth.login("email.qa.test@gmail.com", "password");

        accessToken = response.body.token.accessToken.token;

    });

    it(`Usage is here`, async () => {
        let userData: object = {
            id: 7,
            avatar: "string",
            email: "alex.qa.test@gmail.com",
            userName: "AlexQaNew",
        };

        let response = await users.updateUser(userData, accessToken);
        checkStatusCode(response, 204);
    });

    afterEach(function(){
        console.log('It was a test')
    });
});

