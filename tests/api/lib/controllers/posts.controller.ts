import { ApiRequest } from "../request";

let baseUrl = global.appConfig.baseUrl;
export class PostsController {
    async getAllPosts() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Posts`)
            .send();
        return response;
    }

       async createPost (postData: object, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts`)
            .body(postData)
            .bearerToken(accessToken)
            .send();
        return response;
    }
   
    async likePost (postData: object, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts/like`)
            .body(postData)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async commentPost (postData: object, accessToken: string) {
         const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Comments`)
            .body(postData)
            .bearerToken(accessToken)
            .send();
        return response;
    }
}

