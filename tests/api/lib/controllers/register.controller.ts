import { ApiRequest } from "../request";

let baseUrl = global.appConfig.baseUrl;

export class RegController {
    async registry(userData: object) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`/api/Register`)
            .body(userData)
            .send();
        return response;
    }
}