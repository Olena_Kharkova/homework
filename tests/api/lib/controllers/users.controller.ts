import { ApiRequest } from "../request";
let baseUrl = global.appConfig.baseUrl;

export class UsersController {
    async getAllUsers() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Users`)
            .send();
        return response;
    }

    async getUserById(id) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Users/${id}`)
            .send();
        return response;
    }

    async getUserByToken(accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`/api/Users/fromToken`)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async updateUser(userData: object, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("PUT")
            .url(`api/Users`)
            .body(userData)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async deleteUserById(id) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("DELETE")
            .url(`api/Users/${id}`)
            .send();
        return response;
    }
}